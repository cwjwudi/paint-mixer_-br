PROGRAM _INIT
	(* 配方变量声明 *)
	//程序内读取调度
	RxPaintMix;
	RxPaintMix[0];
	RxPaintMix[1];
	RxPaintMix[2];
	//对外接口
	readRxPaintMix.RxMagent;
	readRxPaintMix.RxYello;
	readRxPaintMix.RxBlack;
	readRxPaintMix.RxStirring_time;	
	writeRxPaintMix.RxCyan;
	writeRxPaintMix.RxMagent;
	writeRxPaintMix.RxYello;
	writeRxPaintMix.RxBlack;
	writeRxPaintMix.RxStirring_time;
	recipeChoose:=0;
	RxLoad;
	RxSave;
	(*功能块的初始化 *)
	//MpRecipeXml
	MpRecipeXml_0.Enable:=FALSE;
	MpRecipeXml_0.MpLink:=ADR(gMpLinkPaMixRecipeXML);
	MpRecipeXml_0.DeviceName:=ADR('CF');
 
	//MpRecipeRegPar_0
	MpRecipeRegPar_0.Enable:=FALSE;
	MpRecipeRegPar_0.MpLink:=ADR(gMpLinkPaMixRecipeXML);
 
	
END_PROGRAM
 
PROGRAM _CYCLIC
	(* 配方存贮读取主逻辑 *)
	CASE STEP OF 
		0://初始化
			MpRecipeRegPar_0.Enable:=FALSE;
			MpRecipeXml_0.Enable:=FALSE;
			MpRecipeXml_0.Save:=FALSE;
			MpRecipeXml_0.Load:=FALSE;
			IF recipeChoose = 1 THEN STEP:=1;//选择配方1..3
			END_IF;
			IF recipeChoose = 2 THEN STEP:=2;
			END_IF;
			IF recipeChoose = 3 THEN STEP:=3;
			END_IF;
		1://选择1号配方
			MpRecipeRegPar_0.PVName:=ADR('Paint_Recipe:RxPaintMix[0]');	
			MpRecipeXml_0.FileName:=ADR('PaintMix_Recipe1');
			MpRecipeRegPar_0.Enable:=TRUE;
			MpRecipeXml_0.Enable:=TRUE;
			STEP:=5;
		2://选择2号配方
			MpRecipeRegPar_0.PVName:=ADR('Paint_Recipe:RxPaintMix[1]');	
			MpRecipeXml_0.FileName:=ADR('PaintMix_Recipe2');
			MpRecipeRegPar_0.Enable:=TRUE;
			MpRecipeXml_0.Enable:=TRUE;
			STEP:=5;		
		3://选择3号配方
			MpRecipeRegPar_0.PVName:=ADR('Paint_Recipe:RxPaintMix[2]');	
			MpRecipeXml_0.FileName:=ADR('PaintMix_Recipe3');
			MpRecipeRegPar_0.Enable:=TRUE;
			MpRecipeXml_0.Enable:=TRUE;
			STEP:=5;		
		5://判断操作为存还是读
			IF RxSave=1 THEN STEP := 610;
			END_IF;
			IF RxLoad=1 THEN STEP :=620;
			END_IF;

 
		610:
			IF recipeChoose = 1 THEN STEP:=611;
			END_IF;
			IF recipeChoose = 2 THEN STEP:=612;
			END_IF;
			IF recipeChoose = 3 THEN STEP:=613;
			END_IF;
		611:
			RxPaintMix[0].RxCyan:=writeRxPaintMix.RxCyan;
			RxPaintMix[0].RxMagent:=writeRxPaintMix.RxMagent;
			RxPaintMix[0].RxYello:= writeRxPaintMix.RxYello;
			RxPaintMix[0].RxBlack:= writeRxPaintMix.RxBlack;
			RxPaintMix[0].RxStirring_time:= writeRxPaintMix.RxStirring_time;
			recipeChoose:=0;
			STEP:=701;
 
		612:
			RxPaintMix[1].RxCyan:=writeRxPaintMix.RxCyan;
			RxPaintMix[1].RxMagent:=writeRxPaintMix.RxMagent;
			RxPaintMix[1].RxYello:= writeRxPaintMix.RxYello;
			RxPaintMix[1].RxBlack:= writeRxPaintMix.RxBlack;
			RxPaintMix[1].RxStirring_time:= writeRxPaintMix.RxStirring_time;
			recipeChoose:=0;
			STEP:=701;
		613:
			RxPaintMix[2].RxCyan:=writeRxPaintMix.RxCyan;
			RxPaintMix[2].RxMagent:=writeRxPaintMix.RxMagent;
			RxPaintMix[2].RxYello:= writeRxPaintMix.RxYello;
			RxPaintMix[2].RxBlack:= writeRxPaintMix.RxBlack;
			RxPaintMix[2].RxStirring_time:= writeRxPaintMix.RxStirring_time;
			recipeChoose:=0;
			STEP:=701;
			//选择读1..3	
		620:
			IF recipeChoose = 1 THEN STEP:=621;
			END_IF;
			IF recipeChoose = 2 THEN STEP:=622;
			END_IF;
			IF recipeChoose = 3 THEN STEP:=623;
			END_IF;
		621:
			readRxPaintMix.RxMagent:=RxPaintMix[0].RxMagent;
			readRxPaintMix.RxYello:=RxPaintMix[0].RxYello;
			readRxPaintMix.RxBlack:=RxPaintMix[0].RxBlack;
			readRxPaintMix.RxCyan:=RxPaintMix[0].RxCyan;
			readRxPaintMix.RxStirring_time:=RxPaintMix[0].RxStirring_time;
			recipeChoose:=0;
			STEP:=702;
		622:
			readRxPaintMix.RxMagent:=RxPaintMix[1].RxMagent;
			readRxPaintMix.RxYello:=RxPaintMix[1].RxYello;
			readRxPaintMix.RxBlack:=RxPaintMix[1].RxBlack;
			readRxPaintMix.RxCyan:=RxPaintMix[1].RxCyan;
			readRxPaintMix.RxStirring_time:=RxPaintMix[1].RxStirring_time;
			recipeChoose:=0;
			STEP:=702;
		623:
			readRxPaintMix.RxMagent:=RxPaintMix[2].RxMagent;
			readRxPaintMix.RxYello:=RxPaintMix[2].RxYello;
			readRxPaintMix.RxBlack:=RxPaintMix[2].RxBlack;
			readRxPaintMix.RxCyan:=RxPaintMix[2].RxCyan;
			readRxPaintMix.RxStirring_time:=RxPaintMix[2].RxStirring_time;
			recipeChoose:=0;
			STEP:=702;
		701://存
			RxSave:=0;
			MpRecipeXml_0.Save:=TRUE;
			IF  MpRecipeXml_0.CommandDone THEN   STEP:= 0;
			END_IF;
		702://读
			RxLoad:=0;
			MpRecipeXml_0.Load:=TRUE;
			IF  MpRecipeXml_0.CommandDone THEN   STEP:= 0;
			END_IF;

	END_CASE
	(* 函数调用 *)
	MpRecipeXml_0();
	MpRecipeRegPar_0();


END_PROGRAM
 
PROGRAM _EXIT
	(* Insert code here *)
END_PROGRAM