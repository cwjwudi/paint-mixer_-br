
{REDUND_ERROR} FUNCTION_BLOCK FBSValveCtrl (*FB  for source tank valve opening*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Execute : {REDUND_UNREPLICABLE} BOOL; (*Execution of the function block begins on a rising edge of this input*)
		MixingTankVolume : {REDUND_UNREPLICABLE} REAL; (*Volume of the mixing tank [L].*)
		FlowRate : {REDUND_UNREPLICABLE} REAL; (*Flow rate through valve [L/s].*)
		Ratio : {REDUND_UNREPLICABLE} REAL; (*Ratio of color [%].*)
		LowLevelSensor : {REDUND_UNREPLICABLE} BOOL; (*Sensor indicating low paint level in tank.*)
	END_VAR
	VAR_OUTPUT
		Done : {REDUND_UNREPLICABLE} BOOL; (*Execution successful. Function block is finished.*)
		Busy : {REDUND_UNREPLICABLE} BOOL; (*Function block is active and must continue to be called.*)
		Error : {REDUND_UNREPLICABLE} BOOL; (*Error occurred during execution.*)
		StatusID : {REDUND_UNREPLICABLE} DINT; (*Status information.*)
		ValvePosition : {REDUND_UNREPLICABLE} BOOL; (*Valve position. 0=OFF, 1=ON*)
		RemainingTime : {REDUND_UNREPLICABLE} REAL; (*Remaining ON time for valve [s].*)
	END_VAR
	VAR
		temp : valveCtrlLTemp_typ;
	END_VAR
END_FUNCTION_BLOCK
