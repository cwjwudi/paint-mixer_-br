
/*!
* @file        FBSValveCtrl.c 
* @brief       源罐阀控程序

?	源罐阀门根据为相应颜色选择的比率打开一段时间。
?	一旦达到主罐的最大容量，就应该开始混合。
?	混合时间结束后，如果下方有罐，则打开排放阀并向其中填充涂料。
?	如果物体检测传感器检测到罐子，则传送带停止，从而罐子位于排出阀下方。物体检测传感器被重置。
?	一旦罐装满（由称重传感器检测到），传送带恢复运动，以便定位下一个罐装罐。
?	如果达到主罐的低液位传感器，则从源罐重新填充。


*	还缺少高低位联锁的代码，S1~S4罐空

* @date        创建于：November 1, 2023/1:13 PM；最后修改日期：November 1, 2023/1:13 PM 
* @version     V1.00.1
* @author      zhaoy 
* @copyright   B&R
*/
/*! History
 *****************************************************************************
 *     2023.11.01    V1.00.1   zhaoy 
 *         (new)     初步实现
 ******************************************************************************
*/
#include <bur/plctypes.h>

#include <brsystem.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "valveCtrlL.h"
#ifdef __cplusplus
	};
#endif

RTInfo_typ	fRTInfo;
REAL	cycT;
 
/* TODO: Add your comment here */
void FBSValveCtrl(struct FBSValveCtrl* inst)
{
	/*TODO: Add your code here*/
	/* get task cycle time */	
	fRTInfo.enable = 1;
	RTInfo(&fRTInfo);
	cycT = (REAL)fRTInfo.cycle_time / 1000;			/* task class cycle time in ms */

	if(inst->FlowRate != 0)
	{
		inst->temp.openTime = inst->MixingTankVolume * inst->Ratio / 100 / inst->FlowRate;
	}

	switch (inst->temp.STEP)
	{
		/*----------------- 初始化---------------*/ 
		case 0:
			inst->ValvePosition = 0;
			inst->temp.timeDelay = 0;
			inst->Error = 0;
			inst->StatusID = 0;
			inst->Busy = 0;
			inst->Done = 0;
			
			if(inst->Execute)
				inst->temp.STEP = 10;
        
			break;
		/*----------------- 计算打开时间---------------*/ 
		case 10:
			if(inst->FlowRate != 0)
			{
				inst->temp.timeDelay = 0;
				inst->temp.STEP = 20;
			}
			else
			{
				/*----------------- 出错，流量为零---------------*/ 
				inst->Error = 1;
				inst->StatusID = 300001;
				inst->temp.STEP = 0;
			}
			break;
		/*----------------- 开始加注---------------*/ 
		case 20:
			if(inst->temp.timeDelay / 1000 >= inst->temp.openTime)
			{
				inst->temp.STEP = 30;
			}
			else
			{
				if(!inst->LowLevelSensor)
				{
					inst->ValvePosition = 1;
					inst->temp.timeDelay += cycT;
					inst->RemainingTime = inst->temp.openTime - ((REAL)inst->temp.timeDelay / 1000);
					inst->Busy = 1;
				}
				else
				{
				}
			}
        
			break;
		/*----------------- 完成---------------*/ 
		case 30: 
			inst->RemainingTime = 0;
			inst->ValvePosition = 0;
			inst->Done = 1;
			inst->Busy = 0;
			
			if(inst->Execute)
			{
				inst->temp.STEP = 10;

				inst->ValvePosition = 0;
				inst->temp.timeDelay = 0;
				inst->Error = 0;
				inst->StatusID = 0;
				inst->Busy = 0;
				inst->Done = 0;
			}
			
			break;

		default:

			break;
	}
     

	
}
