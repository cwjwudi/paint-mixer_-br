
PROGRAM _INIT
	(* Insert code here *)
	(*-----------------------对主罐SVG进行初始化-----------------------*)
	MainTank_Front := '<svg width="110" height="220" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#ff8000" x="0" y="';
	MainTank_Mid := '0';
	MainTank_Tail := '" width="105" height="220" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l105,0l0,217.99999l-105,0l0,-217.99999z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对青色罐SVG进行初始化-----------------------*)
	CyanTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#008080" x="0" y="';
	CyanTank_Mid := '0';
	CyanTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对洋红色罐SVG进行初始化-----------------------*)
	MagentaTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#e4007f" x="0" y="';
	MagentaTank_Mid := '0';
	MagentaTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对黄色罐SVG进行初始化-----------------------*)
	YellowTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#ffff00" x="0" y="';
	YellowTank_Mid := '0';
	YellowTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对黑色罐SVG进行初始化-----------------------*)
	BlackTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#000000" x="0" y="';
	BlackTank_Mid := '0';
	BlackTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对搅拌棒SVG进行初始化-----------------------*)
	Stirrer_Front := '<svg width="105" height="220" xmlns="http://www.w3.org/2000/svg">
	<g id="Layer_1">
	<title>Layer 1</title>
	<line id="svg_1" y2="206.58779" x2="';
	Stirrer_Mid := '51.5';
	Stirrer_Tail := '" y1="1.5" x1="51.5" stroke-width="5" stroke="#000000" fill="none"/>
	</g>

	</svg>';
	
	Stirrer_Compen := 51.5;
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	(******************************************************************************************)
	(***   添加接口   ***) 
	MainTank_Ratio := 0.5;					//主罐液面百分比（小数）
	CyanTank_Ratio := 0.6;					//青色罐液面百分比（小数）
	MagentaTank_Ratio := 0.9;				//洋红色罐液面百分比（小数）
	YellowTank_Ratio;				//黄色罐液面百分比（小数）
	BlackTank_Ratio;				//黑色罐液面百分比（小数）
	Stirrer_Move := 1;					//搅拌棒，置1搅拌
	(******************************************************************************************)
	(*-----------------------对主罐SVG进行处理-----------------------*)
	brsftoa((1-MainTank_Ratio)*220, ADR(MainTank_Mid));
	MainTank_Temp := '';
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Front));
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Mid));
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Tail));
	MainTank_SVG := MainTank_Temp;
	
	(*-----------------------对青色罐SVG进行处理-----------------------*)
	brsftoa((1-CyanTank_Ratio)*110, ADR(CyanTank_Mid));
	CyanTank_Temp := '';
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Front));
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Mid));
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Tail));
	CyanTank_SVG := CyanTank_Temp;
	
	(*-----------------------对洋红色罐SVG进行处理-----------------------*)
	brsftoa((1-MagentaTank_Ratio)*110, ADR(MagentaTank_Mid));
	MagentaTank_Temp := '';
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Front));
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Mid));
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Tail));
	MagentaTank_SVG := MagentaTank_Temp;
	
	(*-----------------------对黄色罐SVG进行处理-----------------------*)
	brsftoa((1-YellowTank_Ratio)*110, ADR(YellowTank_Mid));
	YellowTank_Temp := '';
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Front));
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Mid));
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Tail));
	YellowTank_SVG := YellowTank_Temp;
	
	(*-----------------------对黑色罐SVG进行处理-----------------------*)
	brsftoa((1-BlackTank_Ratio)*110, ADR(BlackTank_Mid));
	BlackTank_Temp := '';
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Front));
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Mid));
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Tail));
	BlackTank_SVG := BlackTank_Temp;
	
	
	(*-----------------------对搅拌棒SVG进行处理-----------------------*)
	IF Stirrer_Move THEN
		(*-----------------------当搅拌棒电机运动的时候进行搅拌-----------------------*)
		IF Stirrer_Compen > 66 THEN
			Stirrer_LR := 0;
		ELSIF Stirrer_Compen < 37 THEN
			Stirrer_LR := 1;
		END_IF
		
		IF Stirrer_LR = 1 THEN
			Stirrer_Compen := Stirrer_Compen + 0.2;
		ELSE
			Stirrer_Compen := Stirrer_Compen - 0.2;
		END_IF
	ELSE
		Stirrer_Compen := 51.5;
	END_IF
	
	brsftoa(Stirrer_Compen, ADR(Stirrer_Mid));
	Stirrer_Temp := '';
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Front));
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Mid));
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Tail));
	Stirrer_SVG := Stirrer_Temp;
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

