
TYPE
	Rxpaintrecipe_typ : 	STRUCT 
		RxCyan : INT(0..100) ;
		RxMagent : INT(0..100) ;
		RxYello : INT(0..100) ;
		RxBlack : INT(0..100) ;
		RxStirring_time : INT;
	END_STRUCT;
END_TYPE
