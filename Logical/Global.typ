
TYPE
	gViewStatus_typ : 	STRUCT 
		MainTank_Ratio : REAL;
		CyanTank_Ratio : REAL;
		MagentaTank_Ratio : REAL;
		YeallowTank_Ratio : REAL;
		BlackTank_Ratio : REAL;
		Stirrer_Move : BOOL;
		Convey_Move : BOOL;
		Cup_Ratio : REAL;
		Cup_Visiable : BOOL;
	END_STRUCT;
	gAlarmStatus_typ : 	STRUCT 
		TriMainTNotFilled : BOOL; (*Color input control active, but main tank doesn't get filled.*)
		TriMainTMax : BOOL; (*Main tank upper level limit reached.*)
		TriMainTMin : BOOL; (*Main tank lower level limit reached.*)
		TriSourceTMin : BOOL; (*Source tank lower level limit reached empty.*)
		TriTimeout : BOOL; (* Timeout for convey or if load cell value doesn��t change.*)
	END_STRUCT;
	gRecipeCtrl_typ : 	STRUCT 
		Load : BOOL;
		Save : BOOL;
		Recipechoose : UINT;
		RYellow : INT;
		RCyan : INT;
		RBlack : INT;
		RMegenta : INT;
		RStirTime : INT;
		WCyan : INT;
		WYellow : INT;
		WBlack : INT;
		WMegenta : INT;
		WStirTime : INT;
	END_STRUCT;
	gMixCmd_typ : 	STRUCT 
		cmdStart : BOOL;
		cmdStop : BOOL;
		cmdResume : BOOL;
	END_STRUCT;
END_TYPE
