
PROGRAM _INIT
	(* Insert code here *)
	(*-----------------------对主罐SVG进行初始化-----------------------*)
	MainTank_Front := '<svg width="110" height="220" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#ff8000" x="0" y="';
	MainTank_Mid := '0';
	MainTank_Tail := '" width="105" height="220" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l105,0l0,217.99999l-105,0l0,-217.99999z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对青色罐SVG进行初始化-----------------------*)
	CyanTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#008080" x="0" y="';
	CyanTank_Mid := '0';
	CyanTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对洋红色罐SVG进行初始化-----------------------*)
	MagentaTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#e4007f" x="0" y="';
	MagentaTank_Mid := '0';
	MagentaTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对黄色罐SVG进行初始化-----------------------*)
	YellowTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#ffff00" x="0" y="';
	YellowTank_Mid := '0';
	YellowTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对黑色罐SVG进行初始化-----------------------*)
	BlackTank_Front := '<svg width="95" height="110" xmlns="http://www.w3.org/2000/svg">
	<g>
	<title>Layer 1</title>
	<path id="svg_5" d="m1031,413" opacity="NaN" stroke="#ffffff" fill="#0f77ff"/>
	<rect fill="#000000" x="0" y="';
	BlackTank_Mid := '0';
	BlackTank_Tail := '" width="95" height="110" id="svg_6" stroke="null"/>
	<path fill="none" opacity="undefined" d="m0.5,0.5l94,0l0,108l-94,0l0,-108z" id="svg_15" stroke-width="5" stroke="#000000"/>
	</g>

	</svg>';
	
	(*-----------------------对搅拌棒SVG进行初始化-----------------------*)
	Stirrer_Front := '<svg width="105" height="220" xmlns="http://www.w3.org/2000/svg">
	<g id="Layer_1">
	<title>Layer 1</title>
	<line id="svg_1" y2="206.58779" x2="';
	Stirrer_Mid := '51.5';
	Stirrer_Tail := '" y1="1.5" x1="51.5" stroke-width="5" stroke="#000000" fill="none"/>
	</g>

	</svg>';
	Stirrer_Compen := 51.5;
	
	(*-----------------------对传送带SVG进行初始化-----------------------*)
	Convey_Front := '<svg width="940" height="120" xmlns="http://www.w3.org/2000/svg">
	<g id="Layer_1">
	<title>Layer 1</title>
	<line id="svg_5" y2="3.5" x2="872.38429" y1="3.5" x1="59" stroke-width="5" stroke="#000000" fill="none"/>
	<line id="svg_6" y2="111.5" x2="879.32368" y1="111.5" x1="62" stroke-width="5" stroke="#000000" fill="none"/>
	<g transform="rotate(';
	Convey_Rotate := '1';
	Convey_Mid := ' 876 56.5)" id="svg_12">
	<ellipse stroke="#000000" ry="54" rx="54" id="svg_4" cy="56.5" cx="876" stroke-width="5" fill="none"/>
	<line id="svg_9" y2="96.43578" x2="908.93578" y1="22.5" x1="835" stroke-width="5" stroke="#000000" fill="none"/>
	<line id="svg_10" y2="94.5" x2="841" y1="20.5" x1="915" stroke-width="5" stroke="#000000" fill="none"/>
	</g>
	<g transform="rotate(';
	Convey_Tail := ' 57 57.5)" id="svg_13">
	<ellipse stroke="#000000" ry="54" rx="54" id="svg_3" cy="57.5" cx="57" stroke-width="5" fill="none"/>
	<line id="svg_7" y2="94.60803" x2="94.10803" y1="20.5" x1="20" stroke-width="5" stroke="#000000" fill="none"/>
	<line id="svg_8" y2="95.00163" x2="18.49837" y1="18.5" x1="95" stroke-width="5" stroke="#000000" fill="none"/>
	</g>
	</g>
	</svg>';
	
	(*-----------------------对杯子SVG进行初始化-----------------------*)
	Cup_Front := '<svg width="80" height="100" xmlns="http://www.w3.org/2000/svg">
	<g id="Layer_1">
	<title>Layer 1</title>
	<rect stroke="#000000" id="svg_1" height="100" width="66" y="-0.5" x="8" stroke-width="5" fill="none"/>
	<rect stroke="null" id="svg_2" height="83" width="66" y="';
	Cup_Mid := '16.5';
	Cup_Tail := '" x="8" stroke-width="5" fill="#ff8000"/>
	</g>
	</svg>';
	
	
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	(******************************************************************************************)
	(***   添加接口   ***) 
    (*November 2, 2023/3:45 PM changed by cuiw : link view status to gViewStatus *)
	MainTank_Ratio := gViewStatus.MainTank_Ratio;					//主罐液面百分比（小数）
	CyanTank_Ratio := gViewStatus.CyanTank_Ratio;					//青色罐液面百分比（小数）
	MagentaTank_Ratio := gViewStatus.MagentaTank_Ratio;				//洋红色罐液面百分比（小数）
	YellowTank_Ratio := gViewStatus.YeallowTank_Ratio;				//黄色罐液面百分比（小数）
	BlackTank_Ratio:= gViewStatus.BlackTank_Ratio;				//黑色罐液面百分比（小数）
	Stirrer_Move := gViewStatus.Stirrer_Move;					//搅拌棒，置1搅拌
	Convey_Move := gViewStatus.Convey_Move;					//传送带电机启动，置1启动
	Cup_Ratio := gViewStatus.Cup_Ratio;							//传送带杯子页面占比（小数）
    Cup_Visiable := gViewStatus.Cup_Visiable;					//杯子是否被运送到指定位置，1为运送到
    (*November 3, 2023/3:32 PM changed by cuiw : link HMIButton to gMixCmd *)
	gMixCmd.cmdResume :=HMIButton.Resume ;						//HMI Resume按钮接口
	gMixCmd.cmdStart := HMIButton.Start;						//HMI Start按钮接口
	gMixCmd.cmdStop := 	HMIButton.Stop; 						//HMI Stop按钮接口
	(******************************************************************************************)
	(*-----------------------对主罐SVG进行处理-----------------------*)
	brsftoa((1-MainTank_Ratio)*220, ADR(MainTank_Mid));
	MainTank_Temp := '';
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Front));
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Mid));
	brsstrcat(ADR(MainTank_Temp), ADR(MainTank_Tail));
	MainTank_SVG := MainTank_Temp;
	
	(*-----------------------对青色罐SVG进行处理-----------------------*)
	brsftoa((1-CyanTank_Ratio)*110, ADR(CyanTank_Mid));
	CyanTank_Temp := '';
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Front));
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Mid));
	brsstrcat(ADR(CyanTank_Temp), ADR(CyanTank_Tail));
	CyanTank_SVG := CyanTank_Temp;
	
	(*-----------------------对洋红色罐SVG进行处理-----------------------*)
	brsftoa((1-MagentaTank_Ratio)*110, ADR(MagentaTank_Mid));
	MagentaTank_Temp := '';
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Front));
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Mid));
	brsstrcat(ADR(MagentaTank_Temp), ADR(MagentaTank_Tail));
	MagentaTank_SVG := MagentaTank_Temp;
	
	(*-----------------------对黄色罐SVG进行处理-----------------------*)
	brsftoa((1-YellowTank_Ratio)*110, ADR(YellowTank_Mid));
	YellowTank_Temp := '';
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Front));
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Mid));
	brsstrcat(ADR(YellowTank_Temp), ADR(YellowTank_Tail));
	YellowTank_SVG := YellowTank_Temp;
	
	(*-----------------------对黑色罐SVG进行处理-----------------------*)
	brsftoa((1-BlackTank_Ratio)*110, ADR(BlackTank_Mid));
	BlackTank_Temp := '';
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Front));
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Mid));
	brsstrcat(ADR(BlackTank_Temp), ADR(BlackTank_Tail));
	BlackTank_SVG := BlackTank_Temp;
	
	
	(*-----------------------对搅拌棒SVG进行处理-----------------------*)
	IF Stirrer_Move THEN
		(*-----------------------当搅拌棒电机运动的时候进行搅拌-----------------------*)
		IF Stirrer_Compen > 66 THEN
			Stirrer_LR := 0;
		ELSIF Stirrer_Compen < 37 THEN
			Stirrer_LR := 1;
		END_IF
		
		IF Stirrer_LR = 1 THEN
			Stirrer_Compen := Stirrer_Compen + 0.2;
		ELSE
			Stirrer_Compen := Stirrer_Compen - 0.2;
		END_IF
	ELSE
		Stirrer_Compen := 51.5;
	END_IF
	
	brsftoa(Stirrer_Compen, ADR(Stirrer_Mid));
	Stirrer_Temp := '';
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Front));
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Mid));
	brsstrcat(ADR(Stirrer_Temp), ADR(Stirrer_Tail));
	Stirrer_SVG := Stirrer_Temp;
	
	(*-----------------------对传送带SVG进行处理-----------------------*)
	IF Convey_Move THEN
		Convey_Compen := (Convey_Compen + 2) MOD 360;
	ELSE
		Convey_Compen := 0;
	END_IF
	
	brsitoa(Convey_Compen, ADR(Convey_Rotate));
	Convey_Temp := '';
	brsstrcat(ADR(Convey_Temp), ADR(Convey_Front));
	brsstrcat(ADR(Convey_Temp), ADR(Convey_Rotate));
	brsstrcat(ADR(Convey_Temp), ADR(Convey_Mid));
	brsstrcat(ADR(Convey_Temp), ADR(Convey_Rotate));
	brsstrcat(ADR(Convey_Temp), ADR(Convey_Tail));
	Convey_SVG := Convey_Temp;
	
	(*-----------------------对杯子SVG进行处理-----------------------*)
	brsftoa((1-Cup_Ratio)*83+16.5, ADR(Cup_Mid));
	Cup_Temp := '';
	brsstrcat(ADR(Cup_Temp), ADR(Cup_Front));
	brsstrcat(ADR(Cup_Temp), ADR(Cup_Mid));
	brsstrcat(ADR(Cup_Temp), ADR(Cup_Tail));
	Cup_SVG := Cup_Temp;
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

