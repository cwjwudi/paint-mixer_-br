
TYPE
	Recipetyp : 	STRUCT 
		Load : BOOL;
		Save : BOOL;
		Recipechoose : UINT;
		RYellow : INT;
		RCyan : INT;
		RBlack : INT;
		RMegenta : INT;
		RStirTime : INT;
		WCyan : INT;
		WYellow : INT;
		WBlack : INT;
		WMegenta : INT;
		WStirTime : INT;
	END_STRUCT;
	Alarmdetect : 	STRUCT 
		TriMainTNotFilled : BOOL;
		TriMainTMax : BOOL;
		TriMainTMin : BOOL;
		TriSourceTMin : BOOL;
		TriTimeout : BOOL;
		AlarmAck : BOOL;
	END_STRUCT;
END_TYPE
