
PROGRAM _INIT
	(* Insert code here *)
	Alarm;
	Recipe;
	
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* 对接程序读写接口 *)
    
    (*November 3, 2023/2:39 PM changed by cuiw : link Alarm to gAlarmstatus *)
    Alarm.TriMainTNotFilled := gAlarmStatus.TriMainTNotFilled;
    Alarm.TriMainTMax := gAlarmStatus.TriMainTMax;
    Alarm.TriMainTMin := gAlarmStatus.TriMainTMin;
    Alarm.TriSourceTMin := gAlarmStatus.TriSourceTMin;
    Alarm.TriTimeout := gAlarmStatus.TriTimeout;
    

	gRecipeCtrl.Save := Recipe.Save;
	gRecipeCtrl.Load := Recipe.Load;
	gRecipeCtrl.WBlack := Recipe.WBlack;
	gRecipeCtrl.WCyan := Recipe.WCyan;
	gRecipeCtrl.WMegenta := Recipe.WMegenta;
	gRecipeCtrl.WYellow := Recipe.WYellow;
	gRecipeCtrl.WStirTime := Recipe.WStirTime;
	
	Recipe.RBlack := gRecipeCtrl.RBlack;
	Recipe.RCyan := gRecipeCtrl.RCyan;
	Recipe.RMegenta := gRecipeCtrl.RMegenta;
	Recipe.RYellow := gRecipeCtrl.RYellow;
	Recipe.RStirTime := gRecipeCtrl.RStirTime;
	 
	MpAlarmXCore_0(MpLink := ADR(gAlarmXCore), Enable := TRUE);	
	MpAlarmXHistory_0(MpLink := ADR(gAlarmXHistory), Enable := TRUE);	
(*TriMainTNotFilled*)	
	IF Alarm.TriMainTNotFilled THEN
		MpAlarmXSet(gAlarmXCore, 'MainTankNotFilled');
	ELSE
		MpAlarmXReset(gAlarmXCore, 'MainTankNotFilled');
	END_IF	
(*TriMainTMax*)	
	IF Alarm.TriMainTMax THEN
		MpAlarmXSet(gAlarmXCore, 'MainTankMaxLim');
	ELSE
		MpAlarmXReset(gAlarmXCore, 'MainTankMaxLim');
	END_IF
(*TriMainTMin*)	
	IF Alarm.TriMainTMin THEN
		MpAlarmXSet(gAlarmXCore, 'MainTankMinLim');
	ELSE
		MpAlarmXReset(gAlarmXCore, 'MainTankMinLim');
	END_IF
(*TriSourceTMin*)
	IF Alarm.TriSourceTMin THEN
		MpAlarmXSet(gAlarmXCore, 'SourceTankMinLim');
	ELSE
		MpAlarmXReset(gAlarmXCore, 'SourceTankMinLim');
	END_IF
	
(*TriTimeout*)		
	IF Alarm.TriTimeout THEN
		MpAlarmXSet(gAlarmXCore, 'Timeout');
	ELSE
		MpAlarmXReset(gAlarmXCore, 'Timeout');
	END_IF	
	
	IF EDGEPOS(Alarm.AlarmAck) THEN
		Alarm.AlarmAck := TRUE;
		Alarm.TriMainTNotFilled := FALSE;
     	Alarm.TriMainTMax:= FALSE;
		Alarm.TriMainTMin:= FALSE;
		Alarm.TriSourceTMin:= FALSE;
		Alarm.TriTimeout:= FALSE;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

