
TYPE
	mixTankCtrl_Par_type : 	STRUCT 
		SouceTankVolume : REAL; (*源罐容量*)
		MixingTankVolume : REAL; (*混合罐容量*)
		tankVolume : INT; (*小罐罐装容量*)
		FlowRateV5 : REAL; (*混合罐出口阀V5流速*)
		FlowRate : REAL; (*原料罐出口阀V1~V5流速*)
	END_STRUCT;
	mixTankCtrl_Receipt_type : 	STRUCT 
		MixTime : DINT; (*混合罐混合时间，S*)
		Ratio1 : REAL; (*原料罐cyan配比%*)
		Ratio2 : REAL; (*原料罐magenta配比%*)
		Ratio3 : REAL; (*原料罐yellow配比%*)
		Ratio4 : REAL; (*原料罐black配比%*)
	END_STRUCT;
	mixTankCtrl_IOSim_type : 	STRUCT  (*Simulate Sensor REAL DataType*)
		S8 : REAL; (*Weight sensor. Slider.*)
		aiVolume1 : REAL; (*Weight sensor. Slider.*)
		aiVolume2 : REAL; (*Weight sensor. Slider.*)
		aiVolume3 : REAL; (*Weight sensor. Slider.*)
		aiVolume4 : REAL; (*Weight sensor. Slider.*)
		aiVolume5 : REAL; (*Weight sensor. Slider.*)
	END_STRUCT;
	mixTankCtrl_IO_type : 	STRUCT 
		S1 : BOOL; (*Low level sensor for T1. Toggle switch.*)
		S2 : BOOL; (*Low level sensor for T2. Toggle switch.*)
		S3 : BOOL; (*Low level sensor for T3. Toggle switch.*)
		S4 : BOOL; (*Low level sensor for T4. Toggle switch.*)
		S5 : BOOL; (*High level sensor for T5. Toggle switch.*)
		S6 : BOOL; (*Low level sensor for T5. Toggle switch.*)
		S7 : BOOL; (*Object detection sensor for cans. Toggle switch.*)
		S8 : INT; (*Weight sensor. Slider.*)
		M1 : BOOL; (*Motor for agitator.*)
		M2 : BOOL; (*Conveyor belt motor.*)
		V1 : BOOL; (*Valve of Source tank for cyan paint.*)
		V2 : BOOL; (*Valve of Source tank for magenta paint.*)
		V3 : BOOL; (*Valve of Source tank for yellow paint.*)
		V4 : BOOL; (*Valve of Source tank for black paint.*)
		V5 : BOOL; (*Valve of Cans*)
		aiVolume1 : INT; (*Weight sensor. Slider.cyan*)
		aiVolume2 : INT; (*Weight sensor. Slider.magenta*)
		aiVolume3 : INT; (*Weight sensor. Slider.yellow*)
		aiVolume4 : INT; (*Weight sensor. Slider.black*)
		aiVolume5 : INT; (*Weight sensor. Slider.Can*)
	END_STRUCT;
	mixTankCtrl_type : 	STRUCT 
		cmdResume : BOOL;
		cmdStart : BOOL;
		cmdStop : BOOL;
		T1 : FBSValveCtrl;
		T2 : FBSValveCtrl;
		T3 : FBSValveCtrl;
		T4 : FBSValveCtrl;
		returnSTEP : UINT;
		STEP : UINT;
		Alarm : ARRAY[0..99]OF BOOL;
		Par : mixTankCtrl_Par_type; (*参数*)
		Receipt : mixTankCtrl_Receipt_type; (*配方*)
		IOSim : mixTankCtrl_IOSim_type; (*仿真IO*)
		IO : mixTankCtrl_IO_type; (*实际硬件IO*)
	END_STRUCT;
END_TYPE
