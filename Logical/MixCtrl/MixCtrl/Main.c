
/*!
* @file        FBSValveCtrl.c 
* @brief       源罐阀控程序

?	源罐阀门根据为相应颜色选择的比率打开一段时间。
?	一旦达到主罐的最大容量，就应该开始混合。
?	混合时间结束后，如果下方有罐，则打开排放阀并向其中填充涂料。
?	如果物体检测传感器检测到罐子，则传送带停止，从而罐子位于排出阀下方。物体检测传感器被重置。
?	一旦罐装满（由称重传感器检测到），传送带恢复运动，以便定位下一个罐装罐。
?	如果达到主罐的低液位传感器，则从源罐重新填充。

* @date        创建于：November 1, 2023/1:13 PM；最后修改日期：November 1, 2023/1:13 PM 
* @version     V1.00.1
* @author      zhaoy 
* @copyright   B&R
*/
/*! History
 *****************************************************************************
 *     2021.05.16    V1.00.1   zhaoy 
 *         (new)     初步实现
 ******************************************************************************
*/
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	/* get task cycle time */	
	fRTInfo.enable = 1;
	RTInfo(&fRTInfo);
	cycT = (REAL)fRTInfo.cycle_time / 1000;			/* task class cycle time in ms */
	
	/*配方初始化赋值 */
	gMixCtrl.Receipt.MixTime 		= 10;
	gMixCtrl.Receipt.Ratio1 		= 10;
	gMixCtrl.Receipt.Ratio2 		= 20;
	gMixCtrl.Receipt.Ratio3 		= 15;
	gMixCtrl.Receipt.Ratio4 		= 30;

	/*参数初始化赋值 */
	gMixCtrl.Par.SouceTankVolume	= 2000;
	gMixCtrl.Par.tankVolume 		= 30;
	gMixCtrl.Par.FlowRate 			= 10;
	gMixCtrl.Par.FlowRate 			= 10;
	gMixCtrl.Par.FlowRate 			= 10;
	gMixCtrl.Par.FlowRate 			= 10;
	gMixCtrl.Par.FlowRateV5 		= 20;
	gMixCtrl.Par.MixingTankVolume	= 300;
	
	
	/*----------------- 不知道什么原因，第一次执行总是有错，因此先都执行一遍---------------*/ 
	gMixCtrl.T1.Execute 			= 1;
	gMixCtrl.T2.Execute 			= 1;
	gMixCtrl.T3.Execute 			= 1;
	gMixCtrl.T4.Execute 			= 1;
	
	/*----------------- 仿真罐容量初始值---------------*/ 
	canInit();
	
}

void canSim();
void canInit();
void canPause();

void _CYCLIC ProgramCyclic(void)
{
	/******************************************************************************************/
	/***   添加接口   ***/ 
	/******************************************************************************************/
	/*----------------- receipt---------------*/ 

	/*
	gMixCtrl.Receipt.MixTime = ;
	gMixCtrl.Receipt.Ratio1 = * 100;
	gMixCtrl.Receipt.Ratio2 = * 100;
	gMixCtrl.Receipt.Ratio3 = * 100;
	gMixCtrl.Receipt.Ratio4 = * 100;
	*/
	
	/*----------------- control---------------*/ 
	/*
	if(gMixCtrl.Par.MixingTankVolume != 0)
		= (REAL)gMixCtrl.IO.aiVolume5 / gMixCtrl.Par.MixingTankVolume;
	if(gMixCtrl.Par.SouceTankVolume != 0)
	{
		= (REAL)gMixCtrl.IO.aiVolume1 / gMixCtrl.Par.SouceTankVolume;
		= (REAL)gMixCtrl.IO.aiVolume2 / gMixCtrl.Par.SouceTankVolume;
		= (REAL)gMixCtrl.IO.aiVolume3 / gMixCtrl.Par.SouceTankVolume;
		= (REAL)gMixCtrl.IO.aiVolume4 / gMixCtrl.Par.SouceTankVolume;
		= gMixCtrl.IO.M1;
		= gMixCtrl.IO.M2;
		= (REAL)M2TimeDelay / 3000;
		= gMixCtrl.IO.S7;
	}
	
	*/
    /*November 3, 2023/3:28 PM changed by cuiw : link gMixCtrl to cmd port */
    gMixCtrl.cmdStart = gMixCmd.cmdStart; 
    gMixCtrl.cmdStop = gMixCmd.cmdStop;
    gMixCtrl.cmdResume = gMixCmd.cmdResume;
    
    /*November 3, 2023/2:57 PM changed by cuiw : use alarm Num */
    /*November 3, 2023/2:29 PM changed by cuiw : link alarm port to gAlarmStatus */
    gAlarmStatus.TriMainTMin = gMixCtrl.Alarm[ALARM_LOW_LEVEL_T5];
    gAlarmStatus.TriMainTMax = gMixCtrl.Alarm[ALARM_HIGH_LEVEL_T5];
    gAlarmStatus.TriSourceTMin = gMixCtrl.Alarm[ALARM_LOW_LEVEL_T1] || gMixCtrl.Alarm[ALARM_LOW_LEVEL_T2]
                              || gMixCtrl.Alarm[ALARM_LOW_LEVEL_T3] || gMixCtrl.Alarm[ALARM_LOW_LEVEL_T4];
    gAlarmStatus.TriTimeout = gMixCtrl.Alarm[ALARM_WEIGHT_METER_FAULT];
    gAlarmStatus.TriMainTNotFilled = gMixCtrl.Alarm[ALARM_MACHINE_PAUSE];
    
    
    /*November 3, 2023/2:26 PM changed by cuiw : link recipet port to gRecipeCtrl */
    gMixCtrl.Receipt.MixTime = gRecipeCtrl.RStirTime;
    gMixCtrl.Receipt.Ratio1 = gRecipeCtrl.RCyan;
    gMixCtrl.Receipt.Ratio2 = gRecipeCtrl.RMegenta;
    gMixCtrl.Receipt.Ratio3 = gRecipeCtrl.RYellow;
    gMixCtrl.Receipt.Ratio4 = gRecipeCtrl.RBlack;
    
    
    /*November 2, 2023/3:40 PM changed by cuiw : link control port to gViewStatus*/
    if(gMixCtrl.Par.MixingTankVolume != 0) {
        gViewStatus.MainTank_Ratio = (REAL)gMixCtrl.IO.aiVolume5 / gMixCtrl.Par.MixingTankVolume;
        /*November 3, 2023/9:28 AM changed by cuiw : avoid main tank ratio over 1.0 */
        if (gViewStatus.MainTank_Ratio >= 1.0) {
            gViewStatus.MainTank_Ratio = 1.0;
        }
    }
    if(gMixCtrl.Par.SouceTankVolume != 0)
    {
        gViewStatus.CyanTank_Ratio = (REAL)gMixCtrl.IO.aiVolume1 / gMixCtrl.Par.SouceTankVolume;
        gViewStatus.MagentaTank_Ratio = (REAL)gMixCtrl.IO.aiVolume2 / gMixCtrl.Par.SouceTankVolume;
        gViewStatus.YeallowTank_Ratio = (REAL)gMixCtrl.IO.aiVolume3 / gMixCtrl.Par.SouceTankVolume;
        gViewStatus.BlackTank_Ratio = (REAL)gMixCtrl.IO.aiVolume4 / gMixCtrl.Par.SouceTankVolume;
        gViewStatus.Stirrer_Move = gMixCtrl.IO.M1;
        gViewStatus.Convey_Move = gMixCtrl.IO.M2;
        gViewStatus.Cup_Ratio  = (REAL)gMixCtrl.IO.S8 / gMixCtrl.Par.tankVolume;
        gViewStatus.Cup_Visiable = gMixCtrl.IO.S7;
    }
	
		//	gMixCtrl.T1.Execute 			= 1;
		gMixCtrl.T1.MixingTankVolume 	= gMixCtrl.Par.MixingTankVolume;
	gMixCtrl.T1.FlowRate 			= gMixCtrl.Par.FlowRate;
	gMixCtrl.T1.Ratio 				= gMixCtrl.Receipt.Ratio1;
	gMixCtrl.T1.LowLevelSensor 		= gMixCtrl.IO.S1;
	gMixCtrl.IO.V1 = gMixCtrl.T1.ValvePosition;
	FBSValveCtrl(&gMixCtrl.T1);
	if(gMixCtrl.T1.Execute == 1)		gMixCtrl.T1.Execute = 0;
	
	//	gMixCtrl.T2.Execute 			= 1;
	gMixCtrl.T2.MixingTankVolume 	= gMixCtrl.Par.MixingTankVolume;
	gMixCtrl.T2.FlowRate 			= gMixCtrl.Par.FlowRate;
	gMixCtrl.T2.Ratio 				= gMixCtrl.Receipt.Ratio2;
	gMixCtrl.T2.LowLevelSensor 		= gMixCtrl.IO.S1;
	gMixCtrl.IO.V2 = gMixCtrl.T2.ValvePosition;
	FBSValveCtrl(&gMixCtrl.T2);
	if(gMixCtrl.T2.Execute == 1)		gMixCtrl.T2.Execute = 0;
	//
	//	gMixCtrl.T3.Execute 			= 1;
	gMixCtrl.T3.MixingTankVolume 	= gMixCtrl.Par.MixingTankVolume;
	gMixCtrl.T3.FlowRate 			= gMixCtrl.Par.FlowRate;
	gMixCtrl.T3.Ratio 				= gMixCtrl.Receipt.Ratio3;
	gMixCtrl.T3.LowLevelSensor 		= gMixCtrl.IO.S1;
	gMixCtrl.IO.V3 = gMixCtrl.T3.ValvePosition;
	FBSValveCtrl(&gMixCtrl.T3);
	if(gMixCtrl.T3.Execute == 1)		gMixCtrl.T3.Execute = 0;
	//
	//	gMixCtrl.T4.Execute 			= 1;
	gMixCtrl.T4.MixingTankVolume 	= gMixCtrl.Par.MixingTankVolume;
	gMixCtrl.T4.FlowRate 			= gMixCtrl.Par.FlowRate;
	gMixCtrl.T4.Ratio 				= gMixCtrl.Receipt.Ratio4;
	gMixCtrl.T4.LowLevelSensor 		= gMixCtrl.IO.S1;
	gMixCtrl.IO.V4 = gMixCtrl.T4.ValvePosition;	
	FBSValveCtrl(&gMixCtrl.T4);
	if(gMixCtrl.T4.Execute == 1)		gMixCtrl.T4.Execute = 0;
	
	/*以下代码暂时不用*/
	if(gMixCtrl.cmdStop == 1)
	{
		gMixCtrl.cmdStop 	= 0;
		gMixCtrl.STEP 		= 100;
	}
	switch (gMixCtrl.STEP)
	{
		case 0:
			if(gMixCtrl.cmdStart)
			{
				gMixCtrl.cmdStart 	= 0;
				//				gMixCtrl.IOSim.aiVolume1 = (REAL)gMixCtrl.Par.MixingTankVolume * gMixCtrl.Receipt.Ratio1 / 100;
				//				gMixCtrl.IOSim.aiVolume2 = (REAL)gMixCtrl.Par.MixingTankVolume * gMixCtrl.Receipt.Ratio2 / 100;
				//				gMixCtrl.IOSim.aiVolume3 = (REAL)gMixCtrl.Par.MixingTankVolume * gMixCtrl.Receipt.Ratio3 / 100;
				//				gMixCtrl.IOSim.aiVolume4 = (REAL)gMixCtrl.Par.MixingTankVolume * gMixCtrl.Receipt.Ratio4 / 100;
				
				gMixCtrl.STEP = 5;
			}
			
			break;
		/**********************?	源罐阀门根据为相应颜色选择的比率打开一段时间**********************/
		case 5:
			{
				gMixCtrl.cmdStart 				= 0;
				gMixCtrl.T1.Execute 			= 1;
				gMixCtrl.T2.Execute 			= 1;
				gMixCtrl.T3.Execute 			= 1;
				gMixCtrl.T4.Execute 			= 1;
				gMixCtrl.STEP = 10;
			}
			
			break;
		/**********************?	源罐阀门根据为相应颜色选择的比率打开一段时间**********************/
		case 10:
			if(!gMixCtrl.Alarm[ALARM_LOW_LEVEL_T1] && !gMixCtrl.Alarm[ALARM_LOW_LEVEL_T2]
				&& !gMixCtrl.Alarm[ALARM_LOW_LEVEL_T3] && !gMixCtrl.Alarm[ALARM_LOW_LEVEL_T4])
			{
				if(	(gMixCtrl.T1.Done == 1 &&	gMixCtrl.T2.Done == 1 
					&& gMixCtrl.T3.Done == 1 &&	gMixCtrl.T4.Done == 1)
					)
				{
					gMixCtrl.STEP = 20;
					timeDelay = 0;
				}
			}
				/*----------------- 源罐液位低，暂停机器---------------*/ 

			else
			{
				gMixCtrl.returnSTEP = gMixCtrl.STEP;
				gMixCtrl.Alarm[ALARM_MACHINE_PAUSE] = 1;
				gMixCtrl.STEP = 50;
			}

			break;

		/**********************开始混合**********************/
		case 20:
			if(timeDelay > gMixCtrl.Receipt.MixTime*1000)
			{
				gMixCtrl.IO.M1 = 0;
				timeDelay = 0;
				gMixCtrl.STEP = 30;
			}
			else
			{
				gMixCtrl.IO.M1 = 1;
				timeDelay += cycT;
			}
			break;

		/**********************罐子通过传送带运输。一旦检测到罐子，传送带就会停止**********************/
		case 30:
			if(gMixCtrl.IO.S7)
			{
				gMixCtrl.IO.M2 = 0;
				gMixCtrl.STEP = 40;
			}
			else
				gMixCtrl.IO.M2 = 1;

			break;

		/**********************称重传感器称量罐头。喷嘴打开，直到称重传感器指示指定的重量。一旦达到指定重量，喷嘴关闭，传送带恢复运动。**********************/
		case 40:
			if(gMixCtrl.IO.S6 == 1)
			{
				gMixCtrl.IO.V5 = 0;
				gMixCtrl.STEP = 5;
			}
			else
			{
				if(!gMixCtrl.Alarm[ALARM_WEIGHT_METER_FAULT])
				{
					if(gMixCtrl.IO.S8 < gMixCtrl.Par.tankVolume)
					{
						gMixCtrl.IO.V5 = 1;
					}
					else
					{
						gMixCtrl.IO.M2 = 1;
						gMixCtrl.IO.V5 = 0;
						gMixCtrl.STEP = 30;/**********************然后传送带带来新的空罐。*****************/
					}
				}
				else
				{
//					gMixCtrl.STEP = ;
				}
				
			}

			break;
		/**********************一旦相应的低液位传感器被激活，暂停，就需要填充源罐。*****************/
		case 50:
			canPause();
			if(gMixCtrl.cmdResume == 1)
			{
				gMixCtrl.cmdResume = 0;
				gMixCtrl.Alarm[ALARM_MACHINE_PAUSE] = 0;
				gMixCtrl.STEP = gMixCtrl.returnSTEP;
			}

			break;
		/**********************停机*****************/
		case 100:
			
			/*----------------- 仿真罐容量初始值---------------*/ 
			canInit();
			gMixCtrl.STEP = 0;

			break;
		/*********  一旦相应的低液位传感器被激活，就需要填充源罐。**********************/
		/**********************等待罐子**********************/
		//		搅拌器存在于主罐中。搅拌器的目的是均匀混合主罐中的内容物。搅拌器仅在主罐满时运行。混合时间可由用户指定。油漆混合后，将其装入罐中。
		//		罐子通过传送带运输。一旦检测到罐子，传送带就会停止，以便 罐子位于主罐的喷嘴下方。
		//		称重传感器称量罐头。喷嘴打开，直到称重传感器指示指定的重量。一旦达到指定重量，喷嘴关闭，传送带恢复运动。然后传送带带来新的空罐。
		//		一旦相应的低液位传感器被激活，就需要填充源罐。
		default:

			break;
	}
	
	/******************************************************************************************/
	/***   联锁关系   ***/ 
	/******************************************************************************************/
	
	/*----------------- 源罐低液位传感器被触发,低液位传感器被触发的源罐的输出阀被关闭---------------*/ 
	if(gMixCtrl.IO.S1)
	{
		gMixCtrl.IO.V1 = 0;
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T1] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T1] = 0;
	
	if(gMixCtrl.IO.S2)
	{
		gMixCtrl.IO.V2 = 0;
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T2] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T2] = 0;
	
	if(gMixCtrl.IO.S3)
	{
		gMixCtrl.IO.V3 = 0;
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T3] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T3] = 0;
	
	if(gMixCtrl.IO.S4)
	{
		gMixCtrl.IO.V4 = 0;
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T4] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T4] = 0;
	/*----------------- 触发主罐高液位传感器,关闭所有源罐的输出阀。---------------*/
	if(gMixCtrl.IO.S5)		
	{
		gMixCtrl.IO.V1 = 0;
		gMixCtrl.IO.V2 = 0;
		gMixCtrl.IO.V3 = 0;
		gMixCtrl.IO.V4 = 0;
		gMixCtrl.Alarm[ALARM_HIGH_LEVEL_T5] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_HIGH_LEVEL_T5] = 0;
	
	if(gMixCtrl.IO.S6)		
	{
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T5] = 1;
	}
	else
		gMixCtrl.Alarm[ALARM_LOW_LEVEL_T5] = 0;
	
	/*----------------- 卸料阀打开后称重传感器值不变---------------*/
	if(gMixCtrl.IO.V5 && oldS8 == gMixCtrl.IO.S8)	
	{
		S8checkTimeDelay += cycT;
		if(S8checkTimeDelay > 1000)
		{
			//?	指示传感器故障
			gMixCtrl.Alarm[ALARM_WEIGHT_METER_FAULT] = 1;
			//?	中止灌装罐头,停止传送带。
			gMixCtrl.cmdStop = 1;
		}
	}
	else
	{
		S8checkTimeDelay = 0;
		gMixCtrl.Alarm[ALARM_WEIGHT_METER_FAULT] = 0;
	}
	oldS8 = gMixCtrl.IO.S8;
	

		
	
	
	/*----------------- 混合罐液位仿真---------------*/ 
	if(simMode)
	{
		canSim();
	}
}

void _EXIT ProgramExit(void)
{

}

void canSim()
{
	if(gMixCtrl.IO.aiVolume5 > (INT)gMixCtrl.Par.MixingTankVolume)
		gMixCtrl.IO.S5 = 1;  /*主水箱加满 */
	else
		gMixCtrl.IO.S5 = 0;
	
	if(gMixCtrl.IO.aiVolume5 < 1)
		gMixCtrl.IO.S6 = 1;
	else
		gMixCtrl.IO.S6 = 0;
	
	gMixCtrl.IO.S1 = gMixCtrl.IO.aiVolume1 < 1;
	gMixCtrl.IO.S2 = gMixCtrl.IO.aiVolume2 < 1;
	gMixCtrl.IO.S3 = gMixCtrl.IO.aiVolume3 < 1;
	gMixCtrl.IO.S4 = gMixCtrl.IO.aiVolume4 < 1;

	
		
		
	if(gMixCtrl.IO.V5 == 1)
	{
		if(gMixCtrl.Par.FlowRateV5 != 0)
		{
			gMixCtrl.IOSim.S8 			+= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRateV5;
			gMixCtrl.IOSim.aiVolume5 	-= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRateV5;
		}
	}
	
	if(gMixCtrl.IO.V1 == 1)
	{
		if(gMixCtrl.Par.FlowRate != 0)
		{
			gMixCtrl.IOSim.aiVolume5 	+= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
			gMixCtrl.IOSim.aiVolume1 	-= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
		}
	}

	if(gMixCtrl.IO.V2 == 1)
	{
		if(gMixCtrl.Par.FlowRate != 0)
		{
			gMixCtrl.IOSim.aiVolume5 	+= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
			gMixCtrl.IOSim.aiVolume2 	-= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
		}
	}

	if(gMixCtrl.IO.V3 == 1)
	{
		if(gMixCtrl.Par.FlowRate != 0)
		{
			gMixCtrl.IOSim.aiVolume5 	+= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
			gMixCtrl.IOSim.aiVolume3 	-= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
		}
	}

	if(gMixCtrl.IO.V4 == 1)
	{
		if(gMixCtrl.Par.FlowRate != 0)
		{
			gMixCtrl.IOSim.aiVolume5 	+= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
			gMixCtrl.IOSim.aiVolume4 	-= (REAL)cycT / 1000 * gMixCtrl.Par.FlowRate;
		}
	}
	if(gMixCtrl.IOSim.S8 < 0)				gMixCtrl.IOSim.S8 = 0;
	if(gMixCtrl.IOSim.aiVolume1 < 0)		gMixCtrl.IOSim.aiVolume1 = 0;
	if(gMixCtrl.IOSim.aiVolume2 < 0)		gMixCtrl.IOSim.aiVolume2 = 0;
	if(gMixCtrl.IOSim.aiVolume3 < 0)		gMixCtrl.IOSim.aiVolume3 = 0;
	if(gMixCtrl.IOSim.aiVolume4 < 0)		gMixCtrl.IOSim.aiVolume4 = 0;
	if(gMixCtrl.IOSim.aiVolume5 < 0)		gMixCtrl.IOSim.aiVolume5 = 0;
	gMixCtrl.IO.S8 				= (INT)gMixCtrl.IOSim.S8;
	gMixCtrl.IO.aiVolume1 		= (INT)gMixCtrl.IOSim.aiVolume1;
	gMixCtrl.IO.aiVolume2 		= (INT)gMixCtrl.IOSim.aiVolume2;
	gMixCtrl.IO.aiVolume3 		= (INT)gMixCtrl.IOSim.aiVolume3;
	gMixCtrl.IO.aiVolume4 		= (INT)gMixCtrl.IOSim.aiVolume4;
	gMixCtrl.IO.aiVolume5 		= (INT)gMixCtrl.IOSim.aiVolume5;
	
	if(gMixCtrl.IO.M2 == 1)
	{
		if(M2TimeDelay < 3000)
		{
			M2TimeDelay += cycT;
			gMixCtrl.IO.S7 = 0;
		}
		else
		{
			M2TimeDelay = 0;
			gMixCtrl.IO.S7 = 1;
			gMixCtrl.IOSim.S8 = 0;
			gMixCtrl.IO.S8 = 0;
		}
			
	}
}

void canInit()
{
	/*----------------- 所有阀门初始化---------------*/ 
	gMixCtrl.IO.V1 = 0;
	gMixCtrl.IO.V2 = 0;
	gMixCtrl.IO.V3 = 0;
	gMixCtrl.IO.V4 = 0;
	gMixCtrl.IO.V5 = 0;
	gMixCtrl.IO.M1 = 0;
	gMixCtrl.IO.M2 = 0;
	/*----------------- 仿真罐容量初始值---------------*/ 
	gMixCtrl.IOSim.aiVolume1 			= gMixCtrl.Par.SouceTankVolume;
	gMixCtrl.IOSim.aiVolume2 			= gMixCtrl.Par.SouceTankVolume;
	gMixCtrl.IOSim.aiVolume3 			= gMixCtrl.Par.SouceTankVolume;
	gMixCtrl.IOSim.aiVolume4 			= gMixCtrl.Par.SouceTankVolume;
	gMixCtrl.IOSim.aiVolume5			= 0;
	for (i = 0; i < 100; i++)
	{
		gMixCtrl.Alarm[i] = 0;
	}
	
}
void canPause()
{
	/*----------------- 所有阀门初始化---------------*/ 
	gMixCtrl.T1.Execute 			= 0;
	gMixCtrl.T2.Execute 			= 0;
	gMixCtrl.T3.Execute 			= 0;
	gMixCtrl.T4.Execute 			= 0;
	gMixCtrl.IO.V1 = 0;
	gMixCtrl.IO.V2 = 0;
	gMixCtrl.IO.V3 = 0;
	gMixCtrl.IO.V4 = 0;
	gMixCtrl.IO.V5 = 0;
	gMixCtrl.IO.M1 = 0;
	gMixCtrl.IO.M2 = 0;
	
}

